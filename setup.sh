#!/bin/bash

# This script is used to setup the environment

# ensure argument is number an larger than 2014

if [ $# -ne 1 ] || [ $1 -lt 2014 ]; then
    echo "Usage: setup.sh $0 <year>"
    exit 1
fi

# replace "aoc-template" with "aoc-<year>" in Cargo.toml
sed -i "s/aoc-template/aoc-$1/g" Cargo.toml

# replace "template" with "<year>" in README.md
sed -i "s/template/$1/g" README.md

# remove all lines after line 17 in README.md
sed -i "17,\$d" README.md

# remove the .git folder
rm -rf .git

# rename the current directory to "aoc-<year>"
dir=$(basename $(pwd))
cd ..
mv $dir aoc-$1
cd aoc-$1

# remove this setup script
rm setup.sh

# initialize a new git repository
git init --initial-branch main
git add .
git commit -m "Initial commit"
